

-- | TODO: comment this module
module Logic.XmlParser
    ( fromString
    , getElement
    , getElement'
    , fromStringValue
    , fromDoubleValue
    , fromIntValue
    , Xml (..)
    , XmlElement  (..)
    , XmlValue (..)
    )
    where

import Data.Time
import Data.List
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Number


data XmlValue = XmlString String
              | XmlInt Int
              | XmlDouble Double
              | XmlDate UTCTime
             deriving (Eq)

fromStringValue :: XmlValue -> String
fromStringValue (XmlString s) = s
fromStringValue _ = error "Not a string."
-- fromValue (XmlInt s) = s
-- fromValue (XmlDouble s) = s
-- fromValue (XmlDate s) = s

fromIntValue :: XmlValue -> Int
fromIntValue (XmlInt x) = x
fromIntValue _ = error "Not an integer."

fromDoubleValue :: XmlValue -> Double
fromDoubleValue (XmlDouble v) = v
fromDoubleValue _ = error "Not a double/float value."

instance Show XmlValue where
  show (XmlString v) = show v
  show (XmlInt v) = show v
  show (XmlDouble v) = show v
  show (XmlDate v) = show v

data XmlElement = XmlNode String [XmlAttr] [XmlElement] | XmlLeaf XmlValue
                deriving (Eq)


instance Show XmlElement where
  show (XmlNode n a e) = "<" ++ n ++ attrs a ++ ">" ++ elems e ++ "</" ++ n ++ ">"
  show (XmlLeaf v) = show v

attrs,elems :: Show a => [a] -> [Char]
attrs a = if null a then "" else " "  ++ unwords (intersperse " "  (map show a)) ++ " "
elems e = if null e then "" else unwords (intersperse "\n" (map show e))


data XmlAttr = XmlAttrString String String
             | XmlAttrInt String Int
             | XmlAttrDouble String Double
             deriving (Eq)

instance Show XmlAttr where
  show (XmlAttrString n v) = n ++ "=\"" ++ v ++ "\""
  show (XmlAttrInt n v) = n ++ "=\"" ++ show v ++ "\""
  show (XmlAttrDouble n v) = n ++ "=\"" ++ show v ++ "\""


data Xml = XmlRoot (Maybe XmlInfo) (Maybe XmlElement)
         deriving (Eq)


instance Show Xml where
  show (XmlRoot i r) = case i of
                      Nothing -> ""
                      Just x -> show x
                    ++
                    case r of
                      Nothing -> ""
                      Just (XmlLeaf x) -> show x
                      Just (XmlNode n a e) -> "<" ++ n ++ attrs a ++ ">\n " ++
                                              unwords (intersperse "\n" $ map show e) ++
                                              "\n</" ++ n ++ ">"

data XmlInfo = XmlInfo String String -- version, encoding
             deriving (Eq)

instance Show XmlInfo where
  show (XmlInfo v e) = "<?xml version=\"" ++ v ++ "\" encoding=\"" ++ e ++ "\" ?>"


getElement :: (String, [Int]) -> Xml -> Maybe XmlElement
getElement _ (XmlRoot _ Nothing) = Nothing
getElement v (XmlRoot _ (Just x)) = if null list then Nothing else Just (head list)
  where list = getElement' v x

getElement' :: (String, [Int]) -> XmlElement -> [XmlElement]
getElement' (n, []) (XmlNode en atr chld)
  | n == en = [XmlNode en atr chld]
  | otherwise = []
getElement' (n, x:xs) (XmlNode _ _ chld) =
  if length chld > x
  then getElement' (n, xs) (chld !! x)
  else []
getElement' (_, _) (XmlLeaf _) = []

test = (XmlRoot Nothing $ Just $ XmlNode "account" []
        [ XmlNode "accountNo" [] [XmlLeaf $ XmlInt 1]
        , XmlNode "amount" [] [XmlLeaf $ XmlDouble 3500.00]
        , XmlNode "firstName" [] [XmlLeaf $ XmlString "Max"]
        , XmlNode "lastName" [] [XmlLeaf $ XmlString "Mustermann"]
        ])


fromString :: String -> Either ParseError Xml
fromString = parse xmlParser "Parse error."

-- test2 :: String -> Either ParseError XmlValue
-- test2 str = parse (parseInfo) "Parse error." str


xmlParser :: Parser Xml
xmlParser = do
  _ <- spaces
  i <- try parseInfo <|> return Nothing
  r <- parseRoot <|> return Nothing
  return (XmlRoot i r)


parseRoot :: Parser (Maybe XmlElement)
parseRoot = do
  p <- parseElement
  return $ Just p


parseInfo :: Parser (Maybe XmlInfo)
parseInfo = do
  _ <- string "<?xml"
  a <- many parseAttrInfo
  _ <- spaces >> string "?>" >> spaces
  let v = case find (\(x,_) -> x == "version") a of
            Nothing -> ""
            Just (_,b) -> b
  let e = case find (\(x,_) -> x == "encoding") a of
            Nothing -> ""
            Just (_,e) -> e
  return $
    if null v && null e
      then Nothing
      else Just $ XmlInfo v e


parseAttrInfo :: Parser (String, String)
parseAttrInfo = do
  _ <- spaces
  n <- many1 alphaNum
  _ <- char '=' >> char '"'
  v <- many1 (noneOf ['"'])
  _ <- char '"'
  _ <- spaces
  return $ (n, v)


parseAttr :: Parser XmlAttr
parseAttr = spaces >>
            (try parseAttrDouble <|> try parseAttrInt <|> try parseAttrString)

parseName = many1 alphaNum

parseAttrDouble :: Parser XmlAttr
parseAttrDouble = do
  n <- parseName
  _ <- char '=' >> char '"'
  v <- floating
  _ <- char '"'
  return $ XmlAttrDouble n v


parseAttrInt :: Parser XmlAttr
parseAttrInt = do
  n <- parseName
  _ <- char '=' >> char '"'
  v <- number 10 digit
  _ <- char '"'
  return $ XmlAttrInt n v


parseAttrString :: Parser XmlAttr
parseAttrString = do
  n <- parseName
  _ <- char '=' >> char '"'
  v <- many1 (noneOf ['"'])
  _ <- char '"'
  return $ XmlAttrString n v

parseElement :: Parser XmlElement
parseElement = -- spaces >>
               (try parseElementFull <|> try parseElementSingle <|>
               do
                 v <- try parseValue
                 return $ XmlLeaf v)


parseElementFull :: Parser XmlElement
parseElementFull = do
  _ <- char '<'
  n <- many1 alphaNum
  a <- many parseAttr
  _ <- char '>'
  _ <- spaces
  v <- many1 parseElement <|> return []
  _ <- spaces
  _ <- string ("</" ++ n ++ ">")
  _ <- spaces
  return $ XmlNode n a v

parseElementSingle :: Parser XmlElement
parseElementSingle = do
  _ <- char '<'
  n <- many1 alphaNum
  a <- many parseAttr
  _ <- spaces
  _ <- char '/'
  _ <- spaces
  _ <- char '>'
  _ <- spaces
  return $ XmlNode n a []

parseValue :: Parser XmlValue
parseValue = -- parseValueDate <|>
             try parseValueDouble <|>
             try parseValueInt <|>
             try parseValueString

-- parseValueDate :: Parser XmlValue
-- parseValueDate = error "date not supported"

parseValueDouble :: Parser XmlValue
parseValueDouble = do
  f <- floating
  return $ XmlDouble f

parseValueInt :: Parser XmlValue
parseValueInt = do
  n <- number 10 digit
  return $ XmlInt n

parseValueString :: Parser XmlValue
parseValueString = do
  n <- many1 (noneOf ['<'])
  return $ XmlString n

