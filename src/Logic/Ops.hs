

-- | TODO: comment this module
module Logic.Ops
    ( transferMoney

    )
    where

import Persistence.Entities

import           Control.Monad.Logger         (runStderrLoggingT)
import           Control.Monad.Trans.Resource (runResourceT)
import           Database.Persist.Sqlite
import           Database.Persist.Quasi
import           Yesod
import Data.Time


transferMoney :: Account -> Account -> Double -> Bool
transferMoney fromAcc toAcc amount = False
  -- runStderrLoggingT $
  --        withSqlitePool "bank.db3" openConnectionCount $
  --          \pool -> liftIO $ do
  --            runResourceT $ flip runSqlPool pool $ do
  --                runMigration migrateAll
  --                -- insert $ Person "Michael" "Snoyman" 26
  --            warp 3000 $ Bank pool


