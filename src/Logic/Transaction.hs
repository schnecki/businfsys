-- {-# LANGUAGE Arrows #-}
-- import Text.XML.HXT.Core
-- import Text.XML.HXT.RelaxNG
-- import Data.Fixed (Centi)
-- import Data.Text (Text, pack)


-- type Comment = Maybe Text
-- data Transaction = Deposit !Text !Centi !Comment
--                  | Withdrawal !Text !Centi !Comment
--                  deriving Show

-- make "deposit" = Deposit
-- make "withdrawal" = Withdrawal
-- make trans = error $ "Invalid transaction type: " ++ trans

-- select = getChildren >>>  hasName "transactions" >>> getChildren >>> isElem

-- transform = proc el -> do
--   trans <- getName -< el
--   account <- getAttrValue "account" -< el
--   amount <- getAttrValue "amount" -< el
--   comment <- withDefault (Just . pack ^<< getText <<< getChildren) Nothing -< el
--   returnA -< (make trans) (pack account) (read amount) comment

-- process = arrIO print

-- handleError = getAttrValue a_source >>> arrIO (putStrLn . ("Error in document: " ++))

-- testFile = readFile "Logic/transactions.xml"


-- main :: IO ()
-- main = do
--   inp <- testFile
--   runX $ readString [ withValidate yes
--                     , withInputEncoding isoLatin1
--                     , withRelaxNG "file:////home/schnecki/Documents/UIBK/7.Semester/BusInfoSys/PS/term%20project/Code/src/Logic/Transaction.rng"] inp
--          >>> ((select >>> transform >>> process) `orElse` handleError)
--   return ()


{-# LANGUAGE Arrows #-}


-- | TODO: comment this module
module Logic.Transaction
    ( transaction
    , withdrawal
    )
    where

import Database.Persist
import Database.Persist.Sqlite
import Database.Persist.TH
import Data.Time
import Control.Monad.IO.Class
import Persistence.Entities
import qualified Database.Esqueleto      as E
import           Database.Esqueleto      ((^.))
import Control.Monad.Trans.Reader
import Persistence.Entities


transaction :: MonadIO m => Key Account -> Key Account -> Double -> String
               -> ReaderT SqlBackend m (Key Transaction)
transaction fromAcc toAcc amount info = do
  time <- liftIO getCurrentTime
  update fromAcc [AccountBalance -=. amount]
  update toAcc [AccountBalance +=. amount]
  insert (Transaction fromAcc toAcc amount time info)


withdrawal :: MonadIO m => Key Account -> Double -> String
              -> ReaderT SqlBackend m (Key Withdrawal)
withdrawal acc amount info = do
  time <- liftIO getCurrentTime
  update acc [AccountBalance -=. amount]
  insert (Withdrawal acc amount time info)
