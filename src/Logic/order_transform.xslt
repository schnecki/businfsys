<?xml version="1.0" encoding="UTF-8" ?>

<!-- New document created with EditiX at Wed Nov 21 16:30:14 CET 2012 -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<purchaseOrder>
			<xsl:attribute name="date">
				<xsl:value-of select="order/date"/>
			</xsl:attribute>
			<xsl:attribute name="number">
				<xsl:value-of select="order/@order-no"/>
			</xsl:attribute>
			<xsl:attribute name="discount-precentage">
				<xsl:value-of select="order/customer/discount-precentage"/>
			</xsl:attribute>
			<xsl:apply-templates select="order/customer"/>
			<xsl:for-each select="order/order-positions/position">
				<xsl:choose>
					<xsl:when test="product/@size='small'">
						<smallOrderItem>
							<xsl:apply-templates select="product"/>
						</smallOrderItem>
					</xsl:when>
					<xsl:when test="product/@size='big'">
						<bigOrderItem>
							<xsl:apply-templates select="product"/>
						</bigOrderItem>
					</xsl:when>
					<xsl:otherwise>
						<orderItem>
							<xsl:apply-templates select="product"/>
						</orderItem>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
			<numberOfItems>
				<xsl:value-of select="count(order/order-positions/position)"/>
			</numberOfItems>
			<totalNetPrice>
				<xsl:value-of select="descendant::currency"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="sum(order/order-positions/position/product/net-price)"/>
			</totalNetPrice>
			<discountedNetPrice>
				<xsl:value-of select="order/customer/currency"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="sum(order/order-positions/position/product/net-price) - (sum(order/order-positions/position/product/net-price) * order/customer/discount-precentage div 100)"/>
			</discountedNetPrice>
			<discount>
				<xsl:value-of select="sum(order/order-positions/position/product/net-price) * order/customer/discount-precentage div 100"/>
			</discount>
		</purchaseOrder>
	</xsl:template>
	<xsl:template match="customer">
		<customerNumber>
			<xsl:value-of select="@customer-no"/>
		</customerNumber>
		<customerName>
			<xsl:value-of select="firstname"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="lastname"/>
		</customerName>
		<xsl:apply-templates select="address"/>
	</xsl:template>
	<xsl:template match="address">
		<customerAddress>
			<xsl:value-of select="street-no"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="street"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="city"/>
		</customerAddress>
	</xsl:template>
	<xsl:template match="product">
		<number>
			<xsl:value-of select="@product-no"/>
		</number>
		<name>
			<xsl:value-of select="name"/>
		</name>
		<grossPrice>
			<xsl:value-of select="preceding::currency"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="net-price + (net-price * vat-percentage div 100)"/>
		</grossPrice>
		<vat>
			<xsl:value-of select="preceding::currency"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="net-price * vat-percentage div 100"/>
		</vat>
	</xsl:template>
</xsl:stylesheet>
