{-# LANGUAGE Arrows, PackageImports #-}


-- | TODO: comment this module
module Logic.Transformations
    ( xsltTransaction
    )
    where

import Text.XML.HXT.XSLT
import Control.Arrow
import "hxt" Text.XML.HXT.Core
import "hxt" Text.XML.HXT.DOM.XmlKeywords
import "hxt-xslt" Text.XML.HXT.XSLT.XsltArrows
import "hxt" Text.XML.HXT.Arrow.XmlState.TraceHandling (withTraceLevel)

transfactionStylesheet = readXSLTDoc [withValidate yes, withInputEncoding utf8]


xsltTransaction xmlDoc = undefined
--   transformed <- process "./Logic/Transaction.xslt" xmlDoc


-- process :: String -> String -> IO [String]
process xslStylesheetPath xmlDoc = do

    -- compile stylesheet

    compiledStyleSheetResults <- runX $
        arr (const xslStylesheetPath)
        >>> readXSLTDoc [ withValidate yes, withInputEncoding utf8] -- withTrace 2
        >>> {- withTraceLevel 2 -} xsltCompileStylesheet

    case compiledStyleSheetResults of
         [] -> return ["error compiling " ++ xslStylesheetPath]
         compiledStyleSheet : _ ->
             -- apply compiled stylesheet to xml doc
             runX $ -- arr (const xmlDocPath)
                 -- >>> readXSLTDoc [ withValidate yes, withInputEncoding utf8] -- withTrace 2
                 xmlDoc
                 >>> xsltApplyStylesheet compiledStyleSheet
                 >>> writeDocumentToString [withOutputEncoding utf8,
                                            withXmlPi yes, withIndent yes]

-- readXSLTDoc from internals of module Text.XML.HXT.XSLT.XsltArrows


-- readXSLTDoc from internals of module Text.XML.HXT.XSLT.XsltArrows

readXSLTDoc :: SysConfigList -> IOSArrow String XmlTree
readXSLTDoc options
    = readFromDocument (options ++ defaultOptions)
    where
    defaultOptions
        = [ withCheckNamespaces yes
          , withValidate        no
          , withPreserveComment no
          ]
