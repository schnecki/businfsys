-- | TODO: comment this module
module Logic.Deposit
    ( deposit
    )
    where


import Database.Persist
import Database.Persist.Sqlite
import Database.Persist.TH
import Data.Time
import Control.Monad.IO.Class
import Persistence.Entities
import qualified Database.Esqueleto      as E
import           Database.Esqueleto      ((^.))
import Control.Monad.Trans.Reader
import Persistence.Entities

deposit :: MonadIO m => Key Account -> Double -> ReaderT SqlBackend m (Key Deposit)
deposit aId deposit = do
  time <- liftIO getCurrentTime
  update aId [AccountBalance +=. deposit]
  insert (Deposit aId deposit time)

