{-# LANGUAGE ViewPatterns               #-}
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE CPP                        #-}


-- | TODO: comment this module
module Persistence.Entities where

import           Database.Persist.Quasi
import           Yesod
import Data.Time

-- This generates entities and database tables.
share [mkPersist sqlSettings, mkMigrate "migrateAll"]
#ifdef FLYCHECK
    $(persistFileWith lowerCaseSettings "../Persistence/models")
#else
    $(persistFileWith lowerCaseSettings "Persistence/models")
    -- $(persistFileWith lowerCaseSettings "src/Persistence/models")
#endif


