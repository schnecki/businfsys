{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE TypeFamilies               #-}

-- | TODO: comment this module
module Main
    ( main
    )
    where

import Persistence.Entities
import Presentation.Controller


import           Control.Monad.Logger         (runStderrLoggingT)
import           Control.Monad.Trans.Resource (runResourceT)
import           Database.Persist.Sqlite
import           Database.Persist.Quasi
import           Yesod
import Data.Time


openConnectionCount :: Int
openConnectionCount = 10


main :: IO ()
main = do
  runStderrLoggingT $
         withSqlitePool "bank.db3" openConnectionCount $
           \pool -> liftIO $ do

             runResourceT $ flip runSqlPool pool $ do
                 runMigration migrateAll
                 cId <- insert customer
                 pId <- insert $ person cId
                 insert $ account cId

                 cId2 <- insert customer2
                 cmpId <- insert $ company cId2
                 insert $ contactPerson cId2
                 insert $ contactPerson cId2
                 insert $ account2 cId2
                 -- id <- insert $ Person 234 "Michael" "Snoyman"
                 --       (fromGregorian 1989 11 30)
                 -- insert $ Account 222.0 id
                 -- insert $ Account 100.0 id
                 -- insert $ Account 0.0 id

             warp 3000 $ Bank pool


  where customer = Customer "karin@asdf.de" "12123" "Zuhause in Deutschland 12345, Deurschland"
        person x = Person x 1234567 "Karin" "Fritz" (fromGregorian 2015 02 01)
        account cId = Account 25317.37 cId

        customer2 = Customer "company@asdf.dd" "111" "Zuhause in Deutschland 12345, Deurschland"
        company x = Company x "DR111445532" "Karin & Co KG" 100000000
        contactPerson x = ContactPerson x "DR111445532" 234 "Karin" "Fritz" "06992122"
                          "karin@asdf.de"
        account2 = Account 22.11

