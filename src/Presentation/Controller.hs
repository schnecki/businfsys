{-# LANGUAGE Arrows #-}
{-# LANGUAGE ViewPatterns               #-}
{-# LANGUAGE ExistentialQuantification  #-}
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE CPP                        #-}

-- | TODO: comment this module
module Presentation.Controller
    ( Bank (..)
    )
    where

import Persistence.Entities
import Misc.Misc
import Logic.XmlParser
import Logic.Deposit
import Logic.Transaction


import Text.Printf
import Database.Persist.Types
import System.Locale (defaultTimeLocale)
import Data.Time.Format (formatTime)
import Data.Function (on)
import Data.List
import Data.List.Split
import Data.Maybe (isJust, fromJust)
import Text.XML.HXT.Core hiding (xread, trace)
import Text.Blaze
import Control.Monad
import           Control.Monad.Logger         (runStderrLoggingT)
import           Control.Monad.Trans.Resource (runResourceT)
import           Database.Persist.Sqlite
import           Database.Persist.Quasi
import           Control.Applicative ((<$>), (<*>))
import           Yesod
import Data.Map (empty)
import Data.Text (pack, unpack)
import Data.Time
import Text.Hamlet.XML
import Text.XML
import qualified Database.Esqueleto      as E
import           Database.Esqueleto      ((^.))
import Data.Monoid
import qualified Data.Conduit.Text as CT
import qualified Data.Conduit.List as CL
import Data.Conduit
import Control.Monad.Trans.Writer.Lazy (Writer)
import qualified Data.Text as T


import Debug.Trace


-- We keep our connection pool in the foundation. At program initialization, we
-- create our initial pool, and each time we need to perform an action we check
-- out a single connection from the pool.
data Bank = Bank ConnectionPool

-- Now we need to define a YesodPersist instance, which will keep track of which
-- backend we're using and how to run an action.
instance YesodPersist Bank where
    type YesodPersistBackend Bank = SqlBackend

    runDB action = do
        Bank pool <- getYesod
        runSqlPool action pool


-- Make the Bank data structure an instance of the Yesod Web Framework.
-- This is the point to set several configurations for the web framework.
instance Yesod Bank


-- We'll create a single route, to access a person. It's a very common
-- occurrence to use an Id type in routes.
mkYesod "Bank" [parseRoutes|
                /                              HomeR        GET
                /account/#AccountId            AccountsR    GET
                /accounts                      AccountsListR GET
                /account/#AccountId/statement  StatementR GET
                /withdrawals                   WithdrawalListR GET
                /withdrawal/#WithdrawalId      WithdrawalR    GET
                /transaction/#TransactionId    TransactionR' GET
                /transactions                  TransactionListR GET
                /transaction                   TransactionR PUT
                /deposit                       DepositR     PUT
                /deposit/#DepositId            DepositR'    GET
                /deposits                      DepositListR GET
                /chart.js                      ChartJsR GET
                           |]


-- header =
--   toWidgetHead [hamlet|
--                 <link rel="stylesheet" type="text/css" href="stylesheet.css">
--                 |]


footer :: HtmlUrl (Route Bank)
footer = [hamlet|
          <footer>
          Return to <a href=@{HomeR}>Home</a>.
                |]


-- TEST CMD: curl -X PUT http://localhost:3000t/ransaction -Hcontent-type:application/xml -d @//home/schnecki/Documents/UIBK/7.Semester/BusInfoSys/PS/term\ project/Code/TestXml.xml
putTransactionR :: Handler TypedContent
putTransactionR =
  selectRep $
    provideRep putTransactionRXml

putTransactionRXml :: Handler RepXml
putTransactionRXml = do
  texts <- rawRequestBody $$ CT.decode CT.ascii =$ CL.consume
  let doc = unlines $ map unpack texts
      mXml = fromString doc

#ifdef DEBUG
  (liftIO . print) doc
#endif

  case mXml of
    Left _ -> errorXml "error" "Could not read file."
    Right xml -> do
      let mTyp = getElement ("withdrawal", [0]) xml

#ifdef DEBUG
      (liftIO . print) xml
      (liftIO . print) mTyp
#endif
      case mTyp of
        Nothing ->               -- not a withdrawal
          case getElement ("wiretransfer", [0]) xml of
            Nothing -> errorXml "error" "Not an instance of the XML schema of transaction."
            Just _ -> do         -- wiretransfer
              let mFrom = getElement ("fromAccount", [0, 0]) xml
                  mTo = getElement ("toAccount", [0, 1]) xml
                  mAmount = getElement ("amount", [0, 2]) xml
                  mInfo = getElement ("info" , [0, 3]) xml

              case mFrom of
                Just (XmlNode "fromAccount" [] [XmlLeaf from]) ->
                  case mTo of
                    Just (XmlNode "toAccount" [] [XmlLeaf to]) ->
                      case mAmount of
                        Just (XmlNode "amount" [] [XmlLeaf amount]) ->
                          case mInfo of
                            Just (XmlNode "info" [] [XmlLeaf info]) -> do
                              runDB $ transaction ((toSqlKey . fromIntegral) $ fromIntValue from)
                                ((toSqlKey . fromIntegral) $ fromIntValue to)
                                (fromDoubleValue amount) (fromStringValue info)
                              repSucc
                            _ -> errorXml "error" "Not an instance of the XML schema of transaction."
                        _ -> errorXml "error" "Not an instance of the XML schema of transaction."
                    _ -> errorXml "error" "Not an instance of the XML schema of transaction."
                _ -> errorXml "error" "Not an instance of the XML schema of transaction."

        Just _ -> do     -- withdrawal
          let mAcc = getElement ("fromAccount", [0,0]) xml
              mAmount = getElement ("amount", [0,1]) xml
              mInfo = getElement ("info", [0,2]) xml
          case mAcc of
            Just (XmlNode "fromAccount" [] [XmlLeaf account]) ->
              case mAmount of
                Just (XmlNode "amount" [] [XmlLeaf amount]) ->
                  case mInfo of
                    Just (XmlNode "info" [] [XmlLeaf info]) -> do
                      runDB $ withdrawal ((toSqlKey . fromIntegral) $ fromIntValue account)
                        (fromDoubleValue amount) (fromStringValue info)
                      repSucc
                    _ -> errorXml "error" "Not an instance of the XML schema of transaction."
                _ -> errorXml "error" "Not an instance of the XML schema of transaction."
            _ -> errorXml "error" "Not an instance of the XML schema of transaction."


getWithdrawalR :: WithdrawalId -> Handler TypedContent
getWithdrawalR id =
  selectRep $ provideRep (getWithdrawalR' id)


getWithdrawalR' :: WithdrawalId -> Handler Html
getWithdrawalR' id = do
  m <- runDB $ selectFirst [WithdrawalId ==. id] []
  case m of
   Nothing -> errorHtml "No data available with this withdrawal id."
   Just (Entity wId wd) -> do
     let aId = withdrawalAccountId wd
     defaultLayout $ do
       setTitle "Withdrawal Details"
       css
       [whamlet|
        <h1>Withdrawal No. #{withdrawalFromId wId}
        <p>
          <b>Account: </b>
            <a href=@{AccountsR aId}>#{accountFromId aId}</a> <br />
          <b>Amount: </b> #{showBalance $ withdrawalAmount wd} <br />
          <b>Date/Time: </b> #{showDatetime $ withdrawalDatetime wd} <br />
          <b>Reference: </b> #{withdrawalReference wd}
          ^{footer}
                       |]

getTransactionR' :: TransactionId -> Handler TypedContent
getTransactionR' id =
  selectRep $ provideRep (getTransactionR'' id)


getTransactionR'' :: TransactionId -> Handler Html
getTransactionR'' id = do
  mTrans <- runDB $ selectFirst [TransactionId ==. id] []
  case mTrans of
   Nothing -> errorHtml "No data available with this transaction id."
   Just (Entity tId trans) -> do
     let fromId = transactionFrom trans
         toId = transactionTo trans
     defaultLayout $ do
       setTitle "Transaction Details"
       css
       [whamlet|
        <h1>Transaction No. #{transactionFromId tId}
        <p>
          <b>Sender Account: </b>
            <a href=@{AccountsR fromId}>#{accountFromId fromId}</a> <br />
          <b>Recipient Account: </b>
            <a href=@{AccountsR toId}>#{accountFromId toId}</a> <br />
          <b>Amount: </b> #{showBalance $ transactionAmount trans} <br />
          <b>Date/Time: </b> #{showDatetime $ transactionDatetime trans} <br />
          <b>Reference: </b> #{transactionReference trans}
          ^{footer}
               |]

errorHtml :: String -> Handler Html
errorHtml str = return [shamlet|
             <h1>#{pack str}
                    |]

repSucc :: Handler RepXml
repSucc = return $ RepXml $ toContent $ renderText def $
                 Document (Prologue [] Nothing [])
                 (Element "requestreply" empty [xml|true|]) []

errorXml :: Name -> String -> Handler RepXml
errorXml rootStr str = return $ RepXml $ toContent $ renderText def $
           Document (Prologue [] Nothing [])
           (Element rootStr empty
            [xml|#{pack str}
                |]) []


showBalance :: Double -> String
showBalance x = formatBalance (printf "%.2f\n" x) ++ " EUR"

formatBalance :: String -> String
formatBalance x = h++t
    where
      sp = break (== '.') x
      h = reverse (intercalate "," $ splitEvery 3 $ reverse $ fst sp)
      t = snd sp

-- -- List all accounts
getHomeR :: HandlerT Bank IO Html
getHomeR = do
  acc <- runDB $ selectList [] [Asc AccountId]
  tra <- runDB $ selectList [] [Asc TransactionId]
  wtd <- runDB $ selectList [] [Asc WithdrawalId]
  cus <- runDB $ selectList [] [Asc CustomerId]
  comp <- runDB $ selectList [] [Asc CompanyId]
  pers <- runDB $ selectList [] [Asc PersonId]
  deps <- runDB $ selectList [] [Asc DepositId]

  let sumBal = sum $ map (\(Entity _ a) -> accountBalance a) acc
      sumTrans = sum $ map (\(Entity _ a) -> transactionAmount a) tra
      sumWth = sum $ map (\(Entity _ a) -> withdrawalAmount a) wtd
      sumDeps = sum $ map (\(Entity _ a) -> depositAmount a) deps
  defaultLayout $ do
    setTitle "Dashboard"
    css
    script
    script2 pers comp sumWth sumDeps
    [whamlet|
                 <center>
                   <h1>Dashboard
                   <table border=1 width=80%>
                     <tr>
                       <th width=33%>Accounts
                       <th width=34%>Transactions
                       <th width=33%>Withdrawals/Deposits
                     <tr>
                       <td>No. of Accounts: #{length acc}
                       <td>No. of Transactions: #{length tra}
                       <td>No. of Withdrawals: #{length wtd}
                     <tr>
                       <td>No. of Customers: #{length cus}
                       <td>
                       <td>No. of Deposits: #{length deps}
                     <tr>
                       <td>  - Companies: #{length comp}
                       <td>
                       <td>
                     <tr>
                       <td>  - Private Persons: #{length pers}
                       <td>
                       <td>Sum of all withdrawals: #
                          <a href=@{WithdrawalListR}>#{showBalance sumWth}
                     <tr>
                       <td>Sum of all balances: #
                          <a href=@{AccountsListR}>#{showBalance sumBal}
                       <td>Sum of transacted money: #
                          <a href=@{TransactionListR}>#{showBalance sumTrans}
                       <td>Sum of all deposits: #
                          <a href=@{DepositListR}>#{showBalance sumDeps}

                   <table border="1" width="80%">
                     <td width="33%" align="center">
                        <div id="canvas-holder1">
                          <canvas id="chart-area1" width="300" height="300"/>
                     <td width="33%" align="center">
                        <div id="canvas-holder2">
                          <canvas id="chart-area2" width="200" height="200"/>
                     <td width="33%" align="center">
                        <div id="canvas-holder3">
                          <canvas id="chart-area3" width="200" height="200"/>


                   <br /><br />
                   <a href=@{AccountsListR}>List of Accounts (#{length acc})<br />
                   <a href=@{TransactionListR}>List of Transactions (#{length tra})<br />
                   <a href=@{WithdrawalListR}>List of Withdrawals (#{length wtd})<br />
                   <a href=@{DepositListR}>List of Deposits (#{length deps})<br />
                   <br />
                   <br />


                   <br />
                <h4>Help: Example API Calls
                <a href="javascript:toggle('test')">Click here to show content</a>
                <div id="test" style="display: none">

                  <ol>
                     <li><b>Transaction</b><br />
                         curl -X PUT http://localhost:3000/transaction --header "accept: text/xml" -Hcontent-type:text/xml -d @TransactionInvocationInstance2.xml <br /><br />
                         Sample Answer: #{ansRR}<br /><br />
                     <li><b>Deposit</b><br />
                         curl -X PUT http://localhost:3000/deposit --header "accept: text/xml" -Hcontent-type:text/xml -d @DepositInstance.xml <br /><br />
                         Sample Answer: #{ansRR}<br /><br />
                     <li><b>Retrieve Account:</b><br />
                         curl -X GET http://localhost:3000/account/2 --header "accept: text/xml" <br /><br />
                         Sample Answer: #{retrAcc}<br /><br />
                        |]

    where
      ansRR :: String
      ansRR = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><requestreply>true</requestreply>"
      retrAcc :: String
      retrAcc = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><account><accountNo>00000002</accountNo><amount>25472.21</amount><firstName>Karin</firstName><lastName>Fritz</lastName></account>"


getDepositListR :: Handler Html
getDepositListR = do
  ds <- runDB $ selectList [] []
  defaultLayout $ do
    setTitle "List of Deposits"
    css
    [whamlet|
     <h1>Deposits (#{length ds})
     <ul>
       $forall Entity dId d <- ds
         <li>
            <a href=@{DepositR' dId}><b>#{depositFromId dId}:</b></a> #
            Deposit of #{showBalance $ depositAmount d} to Account No.
            <a href=@{AccountsR $ depositAccountId d}>#{accountFromId $ depositAccountId d}</a>
     ^{footer}
     |]


getTransactionListR :: Handler Html
getTransactionListR = do
  trans <- runDB $ selectList [] []
  defaultLayout $ do
    setTitle "List of Transactions"
    css
    [whamlet|
     <h1>Transactions (#{length trans})
     <ul>
       $forall Entity tId ta <- trans
          <li>
             <a href=@{TransactionR' tId}><b>#{transactionFromId tId}:</b></a> #
             Transaction over #{showBalance $ transactionAmount ta} from Account No.
             <a href=@{AccountsR $ transactionFrom ta}>#{accountFromId $ transactionFrom ta}</a> to
             Account No. <a href=@{AccountsR $ transactionTo ta}>#{accountFromId $ transactionTo ta}</a>.
     ^{footer}
     |]


getWithdrawalListR :: Handler Html
getWithdrawalListR = do
  wds <- runDB $ selectList [] []
  defaultLayout $ do
    setTitle "List of Widthdrawals"
    css
    [whamlet|
     <h1>Withdrawals (#{length wds})
     <ul>
       $forall Entity wId wd <- wds
         <li>
            <a href=@{WithdrawalR wId}><b>#{withdrawalFromId wId}:</b></a> #
            Withdrawal of #{withdrawalAmount wd} from Account No. <a href=@{AccountsR $ withdrawalAccountId wd}>#{accountFromId $ withdrawalAccountId wd}</a>
     |]


getAccountsListR :: Handler Html
getAccountsListR = do
  acc <- runDB $ selectList [] []
  defaultLayout $ do
    setTitle "List of Accounts"
    css
    [whamlet|
       <h1>Accounts (#{length acc}):
       <ul>
         $forall Entity accId accData <- acc
           <li>
             <a href=@{AccountsR accId}>Account No. #{accountFromId accId} - #{showBalance $ accountBalance accData}
       ^{footer}
              |]

getStatementR :: AccountId -> Handler Html
getStatementR id = do
  m <- runDB $ selectFirst [AccountId ==. id] []
  case m of
    Nothing -> errorHtml "No data available for this account."
    Just (Entity aId acc) -> do
      ts <- runDB $ selectList ([TransactionFrom ==. id] ||.
                                [TransactionTo ==. id]) [Asc TransactionDatetime]
      ds <- runDB $ selectList [DepositAccountId ==. id] [Asc DepositDatetime]
      ws <- runDB $ selectList [WithdrawalAccountId ==. id] [Asc WithdrawalDatetime]
      time <- liftIO getCurrentTime

      let list = sortBy (compare `on` fst) $
                   map (stmtTransaction id) ts ++ map stmtDeposit ds ++ map stmtWithdrawal ws

      defaultLayout $ do
        setTitle "Statement Details"
        css
        [whamlet|
             <h1>Statement of Account No. #{accountFromId id}
             <p>
               $if null list
                   No records to show. <br />
               $else
                   $forall (_, t) <- list
                      ^{t}
               <br />
               <u>Current Balance:</u><br />
               #{showDatetime time}: #
               $if accountBalance acc < 0
                 <font color=#f00>
                   <b>#{showBalance $ accountBalance acc}
               $else
                 <b>#{showBalance $ accountBalance acc}

             Go to account <a href=@{AccountsR id}>details</a>.
             ^{footer}
                |]


stmtWithdrawal (Entity _ (Withdrawal _ amount dt ref)) =
  (dt, [hamlet|
        #{showDatetime dt}: Withdrawal of <font color=#f00>#{showBalance amount}</font>: #{ref}.<br />
        |])


stmtDeposit (Entity _ (Deposit aId amount dt)) =
  (dt, [hamlet|
        #{showDatetime dt}: Deposit of <font color=#0f5>#{showBalance amount}</font>.<br />
        |])


stmtTransaction id (Entity _ (Transaction f t a dt r)) =
  if id == f
     then (dt, [hamlet|
                #{showDatetime dt}: Sent <font color=#f00>#{showBalance a}</font> to Account
                No. <a href=@{AccountsR t}>#{accountFromId t}</a>. <br />
                      |])
     else (dt, [hamlet|
               #{showDatetime dt}: Received <font color=#0f5>#{showBalance a}</font> from Account No.
               <a href=@{AccountsR f}>#{accountFromId f}</a>. <br />
                     |])


getDepositR' :: DepositId -> Handler Html
getDepositR' id = do
  ds <- runDB $ selectFirst [DepositId ==. id] []
  case ds of
    Nothing -> errorHtml "Could not find any data to this deposit number."
    Just (Entity dId d) ->
      defaultLayout $ do
        setTitle "Deposit Details"
        css
        [whamlet|
         <h1>Deposit No. #{depositFromId dId}
         <p><b>Account No.: </b> #{accountFromId $ depositAccountId d}<br />
            <b>Amount: </b> #{showBalance $ depositAmount d}<br />
            <b>Date/Time: </b> #{showDatetime $ depositDatetime d}<br />
         ^{footer}
                |]

putDepositR :: Handler TypedContent
putDepositR =
  selectRep $
    provideRep putDepositRXml


putDepositRXml :: Handler RepXml
putDepositRXml = do

    texts <- rawRequestBody $$ CT.decode CT.ascii =$ CL.consume

    let doc = unlines $ map unpack texts
        mXml = fromString doc

#ifdef DEBUG
    (liftIO . print) $ "A: " ++ show mXml
#endif

    case mXml of
      Left _ -> errorXml "error" "Could not read file."
      Right xml -> do
        let mAccount = getElement ("account", [0]) xml
            mAmount = getElement ("amount", [1]) xml
#ifdef DEBUG
        (liftIO . print) $ "Account: " ++ show mAccount
        (liftIO . print) $ "Amount: " ++ show mAmount
#endif
        case mAccount of
              Nothing -> err
              Just (XmlNode "account" [] [XmlLeaf accElem])  ->
                case mAmount of
                  Nothing -> err
                  Just (XmlNode "amount" [] [XmlLeaf amElem]) -> do
                        runDB $ deposit ((toSqlKey . fromIntegral)
                           (fromIntValue accElem)) (fromDoubleValue amElem)
                        repSucc
                  Just _  -> err
              Just _  -> err

  where err = errorXml "error" "Not an instance of the XML schema of deposit."

getAccountsR :: AccountId -> Handler TypedContent
getAccountsR id =
  selectRep $ do
    provideRep (xmlAccounts id)
    provideRep (htmlAccounts id)

xmlAccounts :: AccountId -> Handler RepXml
xmlAccounts id = do
      mAccounts <- runDB $ selectFirst [AccountId ==. id] []

      case mAccounts of
        Nothing -> errorXml "account" "No data found with this id."
        Just (Entity _ account) -> do
          let cusId = accountCustomerId account
          mCustomer <- runDB $ selectFirst [CustomerId ==. cusId] []

          case mCustomer of
            Nothing ->  errorXml "account" "No contact person found to account. Database corrupted!"
            Just (Entity _ customer) -> do

              mPerson <- runDB $ selectFirst [PersonCustomerId ==. cusId] []

              case mPerson of
                Just (Entity _ person) ->
                  return $ RepXml $ toContent $ renderText def $
                    Document (Prologue [] Nothing [])
                    (Element "account" empty
                     [xml|
                      <accountNo>#{pack $ accountFromId id}
                      <amount>#{pack $ balanceXML $ accountBalance account}
                      <firstName>#{pack $ personFirstName person}
                      <lastName>#{pack $ personLastName person}
                         |]) []

                Nothing -> do
                  mCompany <- runDB $ selectFirst [CompanyCustomerId ==. cusId] []
                  case mCompany of
                    Just (Entity _ company) ->
                      return $ RepXml $ toContent $ renderText def $
                         Document (Prologue [] Nothing [])
                         (Element "account" empty
                          [xml|
                           <companyAccountNo>#{pack $ accountFromId id}
                           <companyAmount>#{pack $ balanceXML $ accountBalance account}
                           <companyName>#{pack $ companyName company}
                              |]) []
                    Nothing -> errorXml "account"
                                "No contact person found to account. Database corrupted!"

  where balanceXML x = printf "%.2f" x


htmlAccounts :: AccountId -> Handler Html
htmlAccounts id = do
      mAccounts <- runDB $ selectFirst [AccountId ==. id] []

      case mAccounts of
        Nothing -> errorHtml "No data available for this account."
        Just (Entity _ account) -> do
          let cusId = accountCustomerId account
          mCustomer <- runDB $ selectFirst [CustomerId ==. cusId] []

          case mCustomer of
            Nothing -> errorHtml "No contact person found to account. Database corrupted!"
            Just (Entity _ customer) -> do

              mPerson <- runDB $ selectFirst [PersonCustomerId ==. cusId] []

              case mPerson of
                Just (Entity _ person) -> defaultLayout $ do
                  setTitle "Account Details"
                  css
                  [whamlet|
                   <h1>Account No. #{accountFromId id}
                   <h4>(<a href=@{StatementR id}>Check Statement of Account</a>)
                   <p><b>Account Holder:</b> #{personFirstName person} #{personLastName person}<br />
                      <b>Social Security No.:</b> #{personSocialSec person}<br />
                      <b>Birthday of Account Holder:</b> #{show $ personBirthday person}<br />
                      <b>Email:</b> #{customerEmail customer}<br />
                      <b>TelNo:</b> #{customerTelNo customer}<br />
                      <b>Address:</b> #{customerAddress customer}<br />
                      <b>Balance:</b> #{showBalance $ accountBalance account}<br />
                   ^{footer}
                          |]


                Nothing -> do
                  mCompany <- runDB $ selectFirst [CompanyCustomerId ==. cusId] []
                  case mCompany of
                    Just (Entity _ company) -> do
                      let vatNo = companyVatNo company
                      contacts <- runDB $ selectList [ContactPersonCustomerId ==. cusId
                                                    ,ContactPersonVatNo ==. vatNo] []
                      defaultLayout $ do
                        setTitle "Account Details"
                        css
                        [whamlet|
                         <h1>Account No. #{accountFromId id}
                         <h4>(<a href=@{StatementR id}>Check Statement of Account</a>)
                         <p><b>Company Name:</b> #{companyName company}<br />
                            <b>VAT No.:</b> #{companyVatNo company}<br />
                            <b>Size of Company:</b> #{companySize company}<br />
                            <b>Email:</b> #{customerEmail customer}<br />
                            <b>TelNo:</b> #{customerTelNo customer}<br />
                            <b>Address:</b> #{customerAddress customer}<br />
                            <b>Balance:</b> #{showBalance $ accountBalance account}<br />
                         <br />
                         <h2>Contact Person(s):
                         <ul>
                           $forall Entity cpId cp <- contacts
                            <li>
                              <b>Name:</b> #{contactPersonFirstName cp} #{contactPersonLastName cp}<br />
                              <b>Social Security No.:</b> #{contactPersonSocialSec cp}<br />
                              <b>Email:</b> #{contactPersonEmail cp}<br />
                              <b>TelNo:</b> #{contactPersonTelNo cp}<br />
                              <b>Balance:</b> #{showBalance $ accountBalance account}<br />
                         ^{footer}
                         |]
                    Nothing -> errorHtml "No contact person found to account. Database corrupted!"

stmtLink :: AccountId -> HtmlUrl (Route Bank)
stmtLink id = [hamlet|
               <a href=@{StatementR id}>Statement</a>
                     |]


accountFromId (AccountKey (SqlBackendKey x)) =
  if length showX < 8
     then replicate (8 - length showX) '0' ++ showX
     else showX
  where showX = show x


transactionFromId (TransactionKey (SqlBackendKey x)) =
  if length showX < 4
     then replicate (4 - length showX) '0' ++ showX
     else showX
  where showX = show x


withdrawalFromId (WithdrawalKey (SqlBackendKey x)) =
  if length showX < 4
     then replicate (4 - length showX) '0' ++ showX
     else showX
  where showX = show x


depositFromId (DepositKey (SqlBackendKey x)) =
  if length showX < 4
     then replicate (4 - length showX) '0' ++ showX
     else showX
  where showX = show x

showDatetime :: UTCTime -> String
showDatetime = formatTime defaultTimeLocale "%F %T"


script = toWidgetHead [hamlet|
                       <script type="text/javascript">
                          function toggle(control){
                            var elem = document.getElementById(control);
                            if(elem.style.display == "none"){
                                elem.style.display = "block";
                            }else{
                                elem.style.display = "none";
                            }
                          }

                      |]

 -- type sig necessary, since sendFile is polymorphic
getChartJsR :: Handler ()
-- Serves "myfile.js" with text/javascript mime-type.
-- Served from /myfile.js as defined above, but your code needn't know that.
getChartJsR = sendFile "text/javascript" "Chart.js"

script2 p c wSum dSum = do
  addScript ChartJsR
  toWidgetHead [hamlet|
                      <script>
                        function toggle(control){var elem = document.getElementById(control);
                                                 if(elem.style.display == "none"){elem.style.display = "block";}else{elem.style.display = "none";}
                                                }


                        var pieData = [{value: #{length c},
                                        color:"#F7464A",
                                         highlight: "#FF5A5E",
                                         label: "Companies"},
                                        {value: #{length p},
                                         color: "#333399",
                                         highlight: "#000066",
                                         label: "Private Persons"}
                                       ];
                         var data2 = { labels: ["Transaction Amount", "Time"],datasets : [{label: "Line Chart",
                                                                                           fillColor: "rgba(220,0,0,0.2)",
                                                                                           strokeColor: "#F7464A",
                                                                                           pointColor: "rgba(220,0,0,1)",
                                                                                           pointStrokeColor: "#fff",
                                                                                           pointHighlightFill: "#fff",
                                                                                           pointHighlightStroke: "rgba(220,220,220,1)",
                                                                                          data: [65, 59, 80, 81, 56, 55, 40]},
                                                                                        ] };
                        var data3 = { labels: ["Sum of all withdrawals", "Sum of all deposits"],datasets : [
                            {fillColor : "#F7464A",
                             strokeColor : "rgba(220,220,220,0.8)",
                             highlightFill: "#FF5A5E",
                             highlightStroke: "rgba(220,220,220,1)",
                             data : [#{wSum}, #{dSum}]},] };

                        window.onload = function(){var ctx1 = document.getElementById("chart-area1").getContext("2d");
                                                   window.myPie = new Chart(ctx1).Pie(pieData);
                                                   var ctx2 = document.getElementById("chart-area2").getContext("2d");
                                                   window.myLineChart = new Chart(ctx2).Line(data2, {responsive : true});
                                                   var ctx3 = document.getElementById("chart-area3").getContext("2d");
                           window.myBar = new Chart(ctx3).Bar(data3, {responsive : true});};

                       |]


css = toWidget
        [lucius|
                   html, body {
                     padding: 15px;
                     margin: 0;
                   }
                   body {
                     color: #325050;
                     background: #fff;
                     font-family: 'Libre Baskerville', sans-serif;
                     font-size: 100%;
                   }

                   a {
                     color: #0d8ba1;
                     -webkit-transition: all 0.25s ease-out;
                     -moz-transition: all 0.25s ease-out;
                     transition: all 0.25s ease-out;
                     text-decoration: none;
                   }

                   h1 {
                           color: #008080;
                           font-size: 300%;
                   }

                   h4 {
                           color: #008080;
                           font-size: 150%;
                   }

                   table {
                           border-style:solid;
                           border-color:#008080;

                   }

                   tr {
                           border-style:none;

                   }

                   th {
                           border-style:none;
                           background-color: #008080;
                           color: #FFFFFF;
                           font-style: bold;

                   }

                   td{
                           border-style:none;

                   }
                      |]
